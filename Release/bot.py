from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from PIL import Image

import os
import qrcode
import json
import time
import base64
import random
import sys
import urllib.request

'''
    Great article on understanding xpaths for selenium
    https://www.guru99.com/xpath-selenium.html
'''

# Extra code is to block notification pop ups. For example, "Facebook wants to send you notifications ALLOW BLOCK"
chrome_options = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications": 2}
chrome_options.add_argument('--user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36"')
chrome_options.add_argument('log-level=3')
chrome_options.add_experimental_option("prefs", prefs)
chrome_options.add_argument("--headless")
driver = webdriver.Chrome('./chromedriver.exe', options=chrome_options)  # Optional argument, if not specified will search path.

def check_exists_by_xpath(xpath):
    try:
        driver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True


'''
    Github
'''


def github(username, password, phone_number, auth_type):
    log.write(time.ctime(time.time()))
    log.write(" ")
    log.write(auth_type)
    log.close()
    try:
        driver.get('https://github.com/login')
        username_box = driver.find_element_by_name('login')
        time.sleep(random.uniform(0.50, 1.5))
        username_box.send_keys(username)
        time.sleep(random.uniform(0.50, 1.5))
        password_box = driver.find_element_by_name('password')
        time.sleep(random.uniform(0.50, 1.5))
        password_box.send_keys(password)
        time.sleep(random.uniform(0.50, 1.5))
        submit_button = driver.find_element_by_name('commit')
        time.sleep(random.uniform(0.50, 1.5))
        submit_button.submit()
        time.sleep(random.uniform(0.50, 1.5))
        driver.get('https://github.com/settings/two_factor_authentication/intro')

        time.sleep(random.uniform(0.50, 1.5))
        if auth_type == 'App':
            driver.find_element_by_xpath(".//button[text()='Set up using an app']").click()
            time.sleep(random.uniform(0.25, 1.5))
            driver.find_element_by_xpath(".//button[contains(@data-ga-click, 'download')]").click()
            time.sleep(random.uniform(0.25, 1.5))
            driver.find_element_by_xpath(".//button[contains(text(), 'Next')]").click()
            time.sleep(random.uniform(3.0, 4.5))
            element = driver.find_elements_by_xpath('.//details-dialog[@aria-label="Your two-factor secret"]//parent::details-dialog//child::div')[1]
            secret = element.get_attribute('innerText')

            time.sleep(random.uniform(0.50, 1.5))

            # Take screenshot of github auth URL.
            element = driver.find_element_by_xpath('.//table[@class="qr-code-table"]')
            location = element.location
            size = element.size
            png = driver.get_screenshot_as_png()  # saves screenshot of entire page

            # uses PIL library to open image in memory
            from io import BytesIO
            im = Image.open(BytesIO(png))

            left = location['x'] + 5
            top = location['y'] + 5
            right = location['x'] + size['width'] - 5
            bottom = location['y'] + size['height'] - 5

            im = im.crop((left, top, right, bottom))  # defines crop points
            im.save('./qr_temp.png')

            with open('./qr_temp.png', "rb") as f:
                qr_img = f.read()
            os.remove('./qr_temp.png')

            my_json = json.dumps([{'Key': 'QrImage', 'Value': json.dumps(
                {'Image': base64.b64encode(qr_img).decode('ascii'), 'TextCode': secret,
                 "Label": "Enter code from 2FA app."})}])
            print(my_json)
            opt = input()

            code_box = driver.find_element_by_xpath('.//input[@id="two-factor-code"]')
            code_box.send_keys(opt)

            time.sleep(random.uniform(0.50, 1.5))
            driver.find_element_by_xpath('.//button[contains(text(),"Enable")]').click()

            print(json.dumps([{'Key': 'End',
                               'Value': '2FA has been set up. Backup codes downloaded to ~/Downloads folder.'}]))
            end = input()
        elif auth_type == "SMS":
            driver.find_element_by_xpath(".//button[text()='Set up using SMS']").click()
            time.sleep(random.uniform(0.25, 1.5))
            driver.find_element_by_xpath(".//button[contains(@data-ga-click, 'download')]").click()
            time.sleep(random.uniform(0.25, 1.5))
            driver.find_element_by_xpath(".//button[contains(text(), 'Next')]").click()
            time.sleep(random.uniform(0.25, 1.5))
            phone_box = driver.find_element_by_xpath(".//input[@name='number']")
            phone_box.send_keys(phone_number)
            time.sleep(random.uniform(0.25, 1.5))
            driver.find_element_by_xpath(".//button[contains(text(), 'Send authentication code')]").click()

            # TODO: Make more automatic with extension.
            time.sleep(random.uniform(0.50, 1.5))
            my_json = json.dumps([{'Key': 'TextData', 'Value': "Enter the six-digit code sent to your phone"}])
            print(my_json)
            code = input()

            time.sleep(random.uniform(0.50, 1.5))
            code_box = driver.find_element_by_xpath('.//input[@id="two-factor-code"]')
            code_box.send_keys(code)

            time.sleep(random.uniform(0.50, 1.5))
            driver.find_element_by_xpath('.//button[contains(text(),"Enable")]').click()

            print(json.dumps([{'Key': 'End', 'Value': '2FA has been set up for ' + phone_number + '. Backup codes downloaded to ~/Downloads folder.'}]))
            end = input()
            return 0
        else:
            return 1
    except Exception as e:
        print(json.dumps([{'Key': 'End', 'Value': str(e)}]))
        end = input()
        return 1


'''
   Google
'''


def google(username, password, phone_number, auth_type):
    log.write(time.ctime(time.time()))
    log.write(" ")
    log.write(auth_type)
    log.close()
    try:
        if auth_type == "Security key" or auth_type == "Phone call":
            errorMessage = "Support is currently unavailable for the following second factor: {}.".format(auth_type)
            print(json.dumps([{'Key': 'End', 'Value': errorMessage}]))
            return 1
        time.sleep(random.uniform(0.50, 1.5))
        driver.get('https://myaccount.google.com/security')

        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_xpath(".//a[@aria-label='Sign in']").click()
        time.sleep(random.uniform(0.50, 1.5))
        user_box = driver.find_element_by_xpath(".//input[@aria-label='Email or phone']")
        time.sleep(random.uniform(0.50, 1.5))
        user_box.send_keys(username)
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_id('identifierNext').click()

        time.sleep(random.uniform(0.50, 1.5))
        pass_box = driver.find_element_by_xpath(".//input[@aria-label='Enter your password']")
        time.sleep(random.uniform(0.50, 1.5))
        pass_box.send_keys(password)
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_id('passwordNext').click()

        time.sleep(random.uniform(3.0, 4.5))
        driver.find_element_by_xpath(".//a[contains(@href, 'two-step-verification')]").click()
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_xpath(".//span[text()='Get started']//parent::span//parent::div").click()

        # TODO may need to add a turn on button here if they have already enabled two factor.

        time.sleep(random.uniform(0.50, 1.5))
        pass_box = driver.find_element_by_xpath(".//input[@aria-label='Enter your password']")
        time.sleep(random.uniform(0.55, 1.5))
        pass_box.send_keys(password)
        time.sleep(random.uniform(0.55, 1.5))
        driver.find_element_by_id('passwordNext').click()

        time.sleep(random.uniform(1.5, 2.5))
        if auth_type == "Google Prompt":
            driver.find_element_by_xpath("//*[@id='yDmH0d']/c-wiz/div/div[3]/c-wiz/div/div/div/div[3]/div[2]/div/div[3]/div/div[3]").click()
            while not check_exists_by_xpath(".//input[@aria-label='Enter a phone number']"):
                time.sleep(1)
            time.sleep(random.uniform(1.0, 1.5))
            pass_box = driver.find_element_by_xpath(".//input[@aria-label='Enter a phone number']")
            time.sleep(random.uniform(1.0, 1.5))
            pass_box.send_keys(phone_number)
            time.sleep(random.uniform(0.50, 1.5))
            driver.find_element_by_xpath(".//span[text()='Send']//parent::span//parent::div").click()
        elif auth_type == "SMS":
            driver.find_element_by_xpath(".//span[text()='Choose another option']//parent::span//parent::div").click()
            time.sleep(random.uniform(0.5, 1.5))
            driver.find_element_by_xpath("//*[@id='yDmH0d']/c-wiz/div/div[3]/c-wiz/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[2]/div[3]").click()
            time.sleep(random.uniform(1.0, 1.5))
            pass_box = driver.find_element_by_xpath(".//input[@aria-label='Enter a phone number']")
            time.sleep(random.uniform(1.0, 1.5))
            pass_box.send_keys(phone_number)
            time.sleep(random.uniform(0.50, 1.5))
            driver.find_element_by_xpath(".//span[text()='Next']//parent::span//parent::div").click()

        time.sleep(random.uniform(0.50, 1.5))
        my_json = json.dumps([{'Key': 'TextData', 'Value': "Google just sent a text message with a verification code to your phone.\nEnter the code."}])
        print(my_json)
        code = input()
        code = ''.join(c for c in code if c.isdigit())
        code_box = driver.find_element_by_xpath('.//input[@aria-label="Enter the code"]')
        time.sleep(random.uniform(0.50, 1.5))
        code_box.send_keys(code)

        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_xpath('.//span[text()="Next"]//parent::span//parent::div').click()

        time.sleep(random.uniform(1.0, 1.5))
        driver.find_element_by_xpath('.//span[text()="Turn on"]//parent::span//parent::div').click()

        time.sleep(random.uniform(0.50, 1.5))
        print(json.dumps([{'Key': 'End', 'Value': '2FA has been set up for Google.'}]))
        end = input()
        return 0
    except Exception as e:
        print(json.dumps([{'Key': 'End', 'Value': str(e)}]))
        end = input()
        return 1


'''
    Pinterest
'''


def pinterest(username, password, phone_number, auth_type):
    log.write(time.ctime(time.time()))
    log.write(" ")
    log.write(auth_type)
    log.close()
    try:
        time.sleep(random.uniform(1.5, 2.5))
        driver.get('https://www.pinterest.com/login/')
        time.sleep(random.uniform(1.5, 2.5))
        username_box = driver.find_element_by_name('id')
        time.sleep(random.uniform(1.5, 2.5))
        username_box.send_keys(username)
        time.sleep(random.uniform(1.5, 2.5))
        password_box = driver.find_element_by_name('password')
        time.sleep(random.uniform(1.5, 2.5))
        password_box.send_keys(password)
        time.sleep(random.uniform(1.5, 2.5))
        driver.find_element_by_class_name('SignupButton').click()
        time.sleep(random.uniform(1.5, 2.5))
        driver.get('https://www.pinterest.com/settings/security/')
        if auth_type == "SMS":
            time.sleep(random.uniform(2.0, 3.5))
            driver.find_element_by_name('mfa_preference').click()
            time.sleep(random.uniform(0.50, 2.5))
            password_box = driver.find_element_by_id('password')
            password_box.send_keys(password)
            time.sleep(random.uniform(0.50, 2.5))
            driver.find_element_by_xpath("//div[contains(text(), 'Next')]").click()
            driver.implicitly_wait(10)
            number = driver.find_element_by_id('phoneNumber')
            number.clear()
            number.send_keys(phone_number)
            time.sleep(random.uniform(0.50, 2.5))
            driver.find_element_by_xpath("//div[contains(text(), 'Next')]").click()
            time.sleep(random.uniform(0.50, 2.5))
            driver.find_element_by_xpath("//div[contains(text(), 'Resend the code')]").click()

            driver.switch_to_active_element
            my_json = json.dumps([{'Key': 'TextData', 'Value': "Now enter the code we just texted to you."}])
            print(my_json)
            code = input()

            time.sleep(random.uniform(1.5, 2.5))
            code_box = driver.find_element_by_xpath('.//input[@id="code"]')
            code_box.send_keys(code) 

            time.sleep(random.uniform(1.5, 2.5))
            driver.find_element_by_xpath('.//div[contains(text(), "Verify")]//parent::button').click()

            import re

            pattern = re.compile(r"[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]")

            driver.find_element_by_xpath('.//div[contains(text(), "Done")]//parent::button').click()

            print(json.dumps([{'Key': 'End', 'Value': '2FA has been set up for Pinterest.'}]))
            end = input()
            return 0
    except Exception as e:
        print(json.dumps([{'Key': 'End', 'Value': str(e)}]))
        end = input()
        return 1


'''
    Reddit
'''


def reddit(username, password, phone_number, auth_type):
    log.write(time.ctime(time.time()))
    log.write(" ")
    log.write(auth_type)
    log.close()
    try:
        if auth_type == 'App':
            while len(driver.find_elements_by_class_name('open-enable-tfa-popup')) == 0: 
                driver.get('https://www.reddit.com/login/')
                time.sleep(random.uniform(0.50, 1.5))
                username_box = driver.find_element_by_name('username')
                username_box.send_keys(username)
                time.sleep(random.uniform(0.50, 1.5))
                password_box = driver.find_element_by_name('password')
                password_box.send_keys(password)
                time.sleep(random.uniform(0.50, 1.5))
                driver.find_element_by_class_name('AnimatedForm__submitButton').click()
                time.sleep(5)  # wait for authentication before redirect
                driver.get('https://www.reddit.com/prefs/update')
            driver.find_element_by_class_name('open-enable-tfa-popup').click()
            driver.switch_to_active_element

            # Somehow there are multiple elements for each element so we manually select which one we want.
            time.sleep(random.uniform(0.50, 1.5))
            driver.find_elements_by_xpath(".//button[text()='my email is correct']")[1].click()
            time.sleep(random.uniform(0.50, 1.5))
            pass_box = driver.find_elements_by_xpath(".//div[@class='enable-tfa-inputs']//input[@type='password']")[1]
            time.sleep(random.uniform(0.50, 1.5))
            pass_box.send_keys(password)
            time.sleep(random.uniform(0.50, 1.5))
            driver.find_elements_by_xpath(".//button[text()='next']")[2].click()

            time.sleep(random.uniform(2.50, 5.5))
            driver.switch_to_active_element
            canvas = driver.find_element_by_xpath(".//p[@class='secret']//parent::div//canvas")
            png = canvas_to_png(canvas)
            secret = driver.find_element_by_xpath(".//p[@class='secret']").get_attribute('innerText')

            my_json = json.dumps([{"Key": "QrImage", "Value": json.dumps({
                "Image": base64.b64encode(png).decode('ascii'), "TextCode": secret, "Label": "Enter code from 2FA app."})}])
            print(my_json)
            opt = input()

            opt_box = driver.find_elements_by_xpath(".//input[@id='otpfield']")[1]
            opt_box.send_keys(opt)
            driver.find_elements_by_xpath(".//button[text()='enable two-factor']")[1].click()

            time.sleep(random.uniform(0.50, 1.5))
            print(json.dumps([{'Key': 'End', 'Value': '2FA has been set up for Reddit.'}]))
            end = input()
            return 0
    except Exception as e:
        print(json.dumps([{'Key': 'End', 'Value': str(e)}]))
        end = input()
        return 1


'''
    Amazon
'''


def amazon(username, password, phone_number, auth_type):
    log.write(time.ctime(time.time()))
    log.write(" ")
    log.write(auth_type)
    log.close()
    try:
        driver.get('http://www.amazon.com/account')
        time.sleep(random.uniform(0.50, 1.5))
        email_box = driver.find_element_by_id("ap_email")
        time.sleep(random.uniform(0.50, 1.5))
        email_box.send_keys(username)
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_id("continue").click()
        time.sleep(random.uniform(0.50, 1.5))
        pass_box = driver.find_element_by_id("ap_password")
        time.sleep(random.uniform(0.50, 1.5))
        pass_box.send_keys(password)
        driver.find_element_by_xpath(".//input[@aria-labelledby='auth-signin-button-announce']").click()

        '''
            !!! CAPTCHA BOX !!!
            - Handles entering incorrect codes and refreshing CAPTCHA's
        '''
        try:
            flag = True
            while flag:
                time.sleep(random.uniform(0.50, 1.5))
                pass_box = driver.find_element_by_id("ap_password")
                pass_box.clear()
                pass_box.send_keys(password)
                captcha_url = driver.find_element_by_xpath(".//img[contains(@alt, 'CAPTCHA')]").get_attribute('src')
                urllib.request.urlretrieve(captcha_url, "./CAPTCHA.png")
                with open("./CAPTCHA.png", "rb") as f:
                    captcha_img = f.read()

                captcha_box = driver.find_element_by_id("auth-captcha-guess")
                captcha = 'n'
                while captcha == "n":
                    my_json = json.dumps([{"Key": "Captcha", "Value": json.dumps(
                        {"Image": base64.b64encode(captcha_img).decode('ascii'), "TextCode": "",
                         "Label": "Enter CAPTCHA"})}])
                    print(my_json)
                    captcha = input()
                    if captcha == "n":
                        driver.find_element_by_id("auth-captcha-refresh-link").click()
                        time.sleep(random.uniform(0.50, 1.5))
                        captcha_url = driver.find_element_by_xpath(".//img[contains(@alt, 'CAPTCHA')]").get_attribute('src')
                        urllib.request.urlretrieve(captcha_url, "./CAPTCHA.png")
                        with open("./CAPTCHA.png", "rb") as f:
                            captcha_img = f.read()
                os.remove("./CAPTCHA.png")
                captcha_box.send_keys(captcha)
                driver.find_element_by_id("signInSubmit").click()

                try:
                    time.sleep(random.uniform(0.50, 1.5))
                    pass_box = driver.find_element_by_id("ap_password")
                    pass_box.send_keys(password)
                    driver.find_element_by_id("signInSubmit").click()
                except NoSuchElementException:
                    flag = False

        except NoSuchElementException:
            pass

        try:
            # Anti Automation Challenge.
            trash = driver.find_element_by_xpath('.//span[text()="Anti-Automation Challenge"]')

            captcha_url = driver.find_element_by_xpath(".//img[contains(@alt, 'captcha')]").get_attribute('src')
            urllib.request.urlretrieve(captcha_url, "./CAPTCHA.png")
            with open("./CAPTCHA.png", "rb") as f:
                captcha_img = f.read()
            os.remove("./CAPTCHA.png")

            captcha_box = driver.find_element_by_xpath(".//input[@type='text']")
            captcha = 'n'
            while captcha == "n":
                my_json = json.dumps([{"Key": "Captcha", "Value": json.dumps({"Image": base64.b64encode(captcha_img).decode('ascii'), "TextCode": "", "Label": "Enter CAPTCHA"})}])
                print(my_json)
                captcha = input()
                if captcha == "n":
                    driver.find_element_by_id(".//a[@data-value='newCaptcha']").click()
                    time.sleep(random.uniform(0.50, 1.5))
                    captcha_url = driver.find_element_by_xpath(".//img[contains(@alt, 'captcha')]").get_attribute('src')
                    urllib.request.urlretrieve(captcha_url, "./CAPTCHA.png")
                    with open("./CAPTCHA.png", "rb") as f:
                        captcha_img = f.read()
                    os.remove("./CAPTCHA.png")
            captcha_box.send_keys(captcha)
            driver.find_element_by_id("signInSubmit").click()

        except NoSuchElementException:
            pass

        # OTP passwords
        try:
            time.sleep(random.uniform(0.50, 1.5))
            driver.find_element_by_id("continue").click()
            otp_box = driver.find_element_by_xpath(".//div[text()='Enter OTP']//parent::div//child::input")
            info_box = driver.find_element_by_xpath(".//span[contains(text(), 'One Time Password (OTP) sent to')]")
            my_json = json.dumps([{"Key": "TextData", "Value": info_box.get_attribute('innerText').strip()}])

            print(my_json)
            opt = input()
            otp_box.send_keys(opt)
            driver.find_element_by_xpath(".//span[text()='Continue']//parent::span//child::input").click()
        except NoSuchElementException:
            pass

        time.sleep(random.uniform(0.50, 1.5))
        driver.get('https://www.amazon.com/gp/css/homepage.html')
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_xpath(".//div[@data-card-identifier='SignInAndSecurity']").click()
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_id("auth-cnep-advanced-security-settings-button").click()
        time.sleep(random.uniform(0.50, 1.5))
        for element in driver.find_elements_by_xpath(".//a[text()='Remove']"):
            element.click()
            time.sleep(random.uniform(0.50, 1.5))
        # removal of previous 2fa methods
        time.sleep(random.uniform(0.50, 1.5))
        if check_exists_by_xpath("//*[@id='auth-send-code']"):
            driver.find_element_by_xpath("//*[@id='auth-send-code']").click()
            time.sleep(random.uniform(0.50, 1.5))
            code_box = driver.find_element_by_xpath("//*[@id='auth-mfa-otpcode']")
            my_json = json.dumps([{'Key': 'TextData', 'Value': "Please enter the verification code you received via text message."}])
            print(my_json)
            code = input()
            code_box.send_keys(code)
            time.sleep(random.uniform(0.50, 1.5))
            driver.find_element_by_xpath("//*[@id='auth-signin-button']").click()
            time.sleep(random.uniform(1.0, 2.0))
            try:
                driver.find_element_by_xpath("//*[@id='confirm-remove-dialog-auto-click-submit']").click()
            except:
                driver.find_element_by_xpath("//*[@id='confirm-removeAuthApp-button']").click()
            time.sleep(random.uniform(0.50, 1.5))
        driver.find_elements_by_xpath(".//a[text()='Get Started']")[1].click()
        time.sleep(random.uniform(0.50, 1.5))

        if auth_type != 'App':
            driver.find_element_by_xpath(".//span[contains(text(), 'Phone number')]//parent::h5//parent::a").click()
            time.sleep(random.uniform(0.50, 1.5))
            # Amazon switchs ID's on you to avoid scrapping!
            try:
                phone_box = driver.find_element_by_xpath(".//input[@name='phoneNumber']")
            except NoSuchElementException:
                phone_box = driver.find_element_by_xpath(".//input[@name='cvf_phone_num']")
            time.sleep(random.uniform(0.50, 1.5))
            phone_box.send_keys(phone_number)
            time.sleep(random.uniform(0.50, 1.5))
            if auth_type == "phone call":
                driver.find_element_by_xpath(".//span[contains(text(), 'Voice delivery')]//parent::label//parent::div").click()
            time.sleep(random.uniform(0.50, 1.5))
            try:
                driver.find_element_by_xpath(".//input[@id='add-phone-form-submit']").click()
            except NoSuchElementException:
                driver.find_element_by_xpath(".//span[text()='Continue']//parent::span//child::input").click()

            time.sleep(random.uniform(0.50, 1.5))
            try:
                code_box = driver.find_element_by_xpath(".//input[@name='code']")
            except NoSuchElementException:
                code_box = driver.find_element_by_xpath(".//input[@name='code']")
            my_json = json.dumps([{'Key': 'TextData', 'Value': "Please enter the verification code you received via text message."}])
            if auth_type == "phone call":
                 my_json = json.dumps([{'Key': 'TextData', 'Value': "Please enter the verification code you received via phone call."}])
            print(my_json)
            code = input()
            code_box.send_keys(code)
            try:
                driver.find_element_by_xpath(".//input[@id='sia-verify-phone-submit']").click()
            except NoSuchElementException:
                driver.find_element_by_xpath(".//input[@name='cvf_action']").click()

            driver.find_element_by_xpath(".//span[contains(text(), 'Authenticator App')]//parent::h5//parent::a").click()
            time.sleep(random.uniform(0.50, 1.5))
            driver.find_element_by_xpath("//*[@id='enable-mfa-form-submit']").click()
            time.sleep(random.uniform(0.50, 1.5))
        elif auth_type == 'App':
            driver.find_element_by_xpath("//*[@id='sia-otp-accordion-totp-header']").click()
            time.sleep(random.uniform(0.50, 1.5))
            secret_code = driver.find_element_by_xpath(".//span[@id='sia-auth-app-formatted-secret']").get_attribute('innerText')
            time.sleep(random.uniform(0.50, 1.5))
            barcode_url = driver.find_element_by_xpath(".//img[contains(@src, 'data:image/png')]").get_attribute('src')
            time.sleep(random.uniform(0.50, 1.5))
            urllib.request.urlretrieve(barcode_url, "./barcode.png")
            with open("./barcode.png", "rb") as f:
                qr_img = f.read()
                time.sleep(random.uniform(0.50, 1.5))
                my_json = json.dumps([{'Key': 'QrImage', 'Value': json.dumps({'Image': base64.b64encode(qr_img).decode('ascii'), 'TextCode': secret_code, "Label": "Enter code from 2FA app."})}])
                print(my_json)
                opt = input()
                time.sleep(random.uniform(0.50, 1.5))
                otp_box = driver.find_elements_by_xpath("//*[@id='ch-auth-app-code-input']")[0]
                time.sleep(random.uniform(0.50, 1.5))
                otp_box.send_keys(opt)
                time.sleep(random.uniform(0.50, 1.5))
                driver.find_element_by_xpath("//*[@id='ch-auth-app-submit-button']").click()
                time.sleep(random.uniform(0.50, 1.5))
                driver.find_element_by_xpath(".//input[@id='enable-mfa-form-submit']").click()
                time.sleep(random.uniform(0.50, 1.5))
            os.remove("./barcode.png")

        print(json.dumps([{'Key': 'End', 'Value': '2FA has been set up for amazon.'}]))
        end = input()
        return 0
        
    except Exception as e:
        print(json.dumps([{'Key': 'End', 'Value': str(e)}]))
        end = input()
        return 1


'''
    Twitter
'''


def twitter(username, password, phone_number, auth_type):
    if auth_type == "Security key" or auth_type == "App":
        errorMessage = "Support is currently unavailable for the following second factor: {}.".format(auth_type)
        print(json.dumps([{'Key': 'End', 'Value': errorMessage}]))
        return 1
    driver.get('https://twitter.com/login')
    time.sleep(random.uniform(1, 1.5))
    username_box = driver.find_element_by_name('session[username_or_email]')
    username_box.send_keys(username)
    password_box = driver.find_element_by_name('session[password]')
    password_box.send_keys(password)
    driver.find_element_by_xpath(".//div[@data-testid='LoginForm_Login_Button']").click()
    time.sleep(random.uniform(0.5, 1.5))
    driver.get('https://twitter.com/settings/account/login_verification')
    time.sleep(random.uniform(0.5, 1.5))
    driver.find_element_by_xpath(".//span[contains(text(), 'Text message')]//parent::div//parent::label").click()
    time.sleep(random.uniform(0.5, 1.5))
    driver.get('https://twitter.com/account/access?feature=two_factor_auth_sms_enrollment')
    time.sleep(random.uniform(1, 1.5))
    try:
            driver.find_element_by_class_name("EdgeButton").click()
    except:
            pass
    try:
            password_box = driver.find_element_by_name('password')
            password_box.send_keys(password)
            driver.find_element_by_class_name("EdgeButton").click()
            time.sleep(random.uniform(1, 1.2))
    except:
            pass
    try:
            phone_box = driver.find_element_by_name('phone_number')
            phone_box.send_keys(phone_number)
            driver.find_element_by_name('discoverable_by_mobile_phone').click()
            driver.find_element_by_class_name("EdgeButton").click()
            time.sleep(random.uniform(1, 1.2))
    except:
            pass
    driver.find_element_by_class_name("EdgeButton").click()
    my_json = json.dumps([{'Key': 'TextData', 'Value': "Enter the confirmation code sent to your phone."}])
    print(my_json)
    code = input()
    code_box = driver.find_element_by_id('code')
    code_box.send_keys(code)
    driver.find_element_by_class_name("EdgeButton").click()
    print(json.dumps([{'Key': 'End', 'Value': '2FA has been set up for Twitter.'}]))
    return 0


'''
    Util function for converting HTML canvas to png.
'''


def canvas_to_png(element):
    canvas_base64 = driver.execute_script("return arguments[0].toDataURL('image/png').substring(21);", element)
    png_image = base64.b64decode(canvas_base64)
    with open("./bar_resize.png", "wb") as f:
        f.write(png_image)

    im = Image.open("./bar_resize.png")

    im_resize = im.resize((128, 128))
    im_resize.save("./bar_resized.png", "PNG")

    with open("./bar_resized.png", "rb") as f:
        png_image = f.read()

    im.close()
    os.remove("./bar_resized.png")
    os.remove("./bar_resize.png")

    return png_image


def send_keys(element, keys, lower_bound=0.05, upper_bound=.30):
    for key in keys:
        element.send_keys(key)
        time.sleep(random.uniform(lower_bound, upper_bound))


def test(username, password, phone_number, auth_method):
    with open("./barcode.png", "rb") as f:
        qr_img = f.read()
        time.sleep(random.uniform(0.50, 1.5))
        secret_code = "Apples jkfdsklfjklsdjfklsdjfklsdjklfjsdklfjsdkljfklsdjfklsdjfklsdjfklsdjfklsjdklfjslk"
        my_json = json.dumps([{'Key': 'QrImage', 'Value': json.dumps(
            {'Image': base64.b64encode(qr_img).decode('ascii'), 'TextCode': secret_code,
             "Label": "Enter code from 2FA app."})}])
        print(my_json)
        opt = input()
